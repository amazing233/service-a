package com.kgc.cn.controller;


import com.kgc.cn.model.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * created on 2020/1/3 0003.
 */
@RestController
@RequestMapping("/test")
public class BaseController {

    @PostMapping("/model")
    public Model show(@RequestBody Model model){
        return model;
    }
}
